﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ColorVariable : ScriptableObject
{
    public ColorType.ColorList value;
}